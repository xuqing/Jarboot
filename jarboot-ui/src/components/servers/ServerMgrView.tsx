import * as React from "react";
import {Result, Tag, Input, Space, Button} from "antd";
import ServerMgrService, {ServerRunning} from "@/services/ServerMgrService";
import CommonNotice, {notSelectInfo} from '@/common/CommonNotice';
import {
    SyncOutlined, CaretRightOutlined, BorderOutlined, CaretRightFilled, DashboardOutlined,
    PoweroffOutlined, ReloadOutlined, UploadOutlined, LoadingOutlined, SearchOutlined
} from '@ant-design/icons';
import {JarBootConst} from '@/common/JarBootConst';
import {MsgData} from "@/common/WsManager";
import Logger from "@/common/Logger";
import {formatMsg} from "@/common/IntlFormat";
import {PUB_TOPIC, SuperPanel, pubsub} from "@/components/servers";
import OneClickButtons from "@/components/servers/OneClickButtons";
import CommonTable from "@/components/table";
import UploadFileModal from "@/components/servers/UploadFileModal";
import StringUtil from "@/common/StringUtil";
// @ts-ignore
import Highlighter from 'react-highlight-words';
import styles from "./index.less";

export default class ServerMgrView extends React.PureComponent {
    state = {loading: false, data: [] as ServerRunning[], uploadVisible: false,
        selectedRowKeys: [] as string[], searchText: '', searchedColumn: '', filteredInfo: null as any,
        selectRows: [] as ServerRunning[], current: '', oneClickLoading: false};
    height = window.innerHeight - 120;
    searchInput = {} as any;
    componentDidMount() {
        this.refreshServerList(true);
        pubsub.submit(PUB_TOPIC.ROOT, PUB_TOPIC.RECONNECTED, this.refreshServerList);
        pubsub.submit(PUB_TOPIC.ROOT, PUB_TOPIC.WORKSPACE_CHANGE, this.refreshServerList);
        pubsub.submit(PUB_TOPIC.ROOT, PUB_TOPIC.STATUS_CHANGE, this.onStatusChange);
    }

    componentWillUnmount() {
        pubsub.unSubmit(PUB_TOPIC.ROOT, PUB_TOPIC.RECONNECTED, this.refreshServerList);
        pubsub.unSubmit(PUB_TOPIC.ROOT, PUB_TOPIC.WORKSPACE_CHANGE, this.refreshServerList);
        pubsub.unSubmit(PUB_TOPIC.ROOT, PUB_TOPIC.STATUS_CHANGE, this.onStatusChange);
    }

    private activeConsole(sid: string) {
        let data: ServerRunning[] = this.state.data;
        const index = data.findIndex(row => row.sid === sid);
        if (-1 !== index) {
            const selectedRowKeys: any = [data[index].sid];
            const selectRows: any = [data[index]];
            this.setState({selectedRowKeys, selectRows});
        }
    }

    private onStatusChange = (data: MsgData) => {
        const item = this.state.data.find(value => value.sid === data.sid);
        if (!item) {
            return;
        }
        const status = data.body;
        const key = data.sid;
        const server = item.name as string;
        switch (status) {
            case JarBootConst.MSG_TYPE_START:
                // 激活终端显示
                this.activeConsole(key);
                Logger.log(`${server}启动中...`);
                pubsub.publish(key, JarBootConst.START_LOADING);
                this.clearDisplay(item);
                this.updateServerStatus(item, JarBootConst.STATUS_STARTING);
                break;
            case JarBootConst.MSG_TYPE_STOP:
                Logger.log(`${server}停止中...`);
                pubsub.publish(key, JarBootConst.START_LOADING);
                this.updateServerStatus(item, JarBootConst.STATUS_STOPPING);
                break;
            case JarBootConst.MSG_TYPE_START_ERROR:
                Logger.log(`${server}启动失败`);
                CommonNotice.error(`Start ${server} failed!`);
                this.updateServerStatus(item, JarBootConst.STATUS_STOPPED);
                break;
            case JarBootConst.MSG_TYPE_STARTED:
                Logger.log(`${server}启动成功`);
                pubsub.publish(key, JarBootConst.FINISH_LOADING);
                this.updateServerStatus(item, JarBootConst.STATUS_STARTED)
                break;
            case JarBootConst.MSG_TYPE_STOP_ERROR:
                Logger.log(`${server}停止失败`);
                CommonNotice.error(`Stop ${server} failed!`);
                this.updateServerStatus(item, JarBootConst.STATUS_STARTED);
                break;
            case JarBootConst.MSG_TYPE_STOPPED:
                Logger.log(`${server}停止成功`);
                pubsub.publish(key, JarBootConst.FINISH_LOADING);
                this.updateServerStatus(item, JarBootConst.STATUS_STOPPED)
                break;
            case JarBootConst.MSG_TYPE_RESTART:
                Logger.log(`${server}重启成功`);
                pubsub.publish(key, JarBootConst.FINISH_LOADING);
                this.updateServerStatus(item, JarBootConst.STATUS_STARTED)
                break;
            default:
                break;
        }
    };

    private updateServerStatus(server: ServerRunning, status: string) {
        server.status = status;
        this.setState({data: [...this.state.data]});
    }

    getColumnSearchProps = (dataIndex: string) => ({
        // @ts-ignore
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={node => {
                        this.searchInput = node;
                    }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ marginBottom: 8, display: 'block' }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined />}
                        size="small"
                        style={{ width: 90 }}
                    >
                        {formatMsg('SEARCH_BTN')}
                    </Button>
                    <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
                        {formatMsg('RESET_BTN')}
                    </Button>
                    <Button
                        type="link"
                        size="small"
                        onClick={() => {
                            confirm({ closeDropdown: false });
                            this.setState({
                                searchText: selectedKeys[0],
                                searchedColumn: dataIndex,
                            });
                        }}
                    >
                        {formatMsg('FILTER_BTN')}
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: (filtered: boolean) => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
        onFilter: (value: string, record: any) =>
            record[dataIndex]
                ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
                : '',
        onFilterDropdownVisibleChange: (visible: boolean) => {
            if (visible) {
                setTimeout(() => this.searchInput.select(), 100);
            }
        },
        render: (text: string) =>
            this.state.searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                    searchWords={[this.state.searchText]}
                    autoEscape
                    textToHighlight={text ? text.toString() : ''}
                />
            ) : (
                text
            ),
    });

    handleSearch = (selectedKeys: string[], confirm: () => void, dataIndex: string) => {
        confirm();
        this.setState({
            searchText: selectedKeys[0],
            searchedColumn: dataIndex,
        });
    };

    handleReset = (clearFilters: () => void) => {
        clearFilters();
        this.setState({ searchText: '' });
    };

    handleChange = (pagination: any, filters: any, sorter: any) => {
        this.setState({filteredInfo: filters});
    };

    clearFilters = () => {
        this.setState({ filteredInfo: null });
    };

    clearAll = () => {
        this.setState({filteredInfo: null, sortedInfo: null,});
    };

    private getTbProps() {
        return {
            columns: [
                {
                    title: formatMsg('NAME'),
                    dataIndex: 'name',
                    key: 'name',
                    ellipsis: true,
                    sorter: (a: ServerRunning, b: ServerRunning) => a.name.localeCompare(b.name),
                    sortDirections: ['descend', 'ascend'],
                    ...this.getColumnSearchProps('name')
                },
                {
                    title: formatMsg('STATUS'),
                    dataIndex: 'status',
                    key: 'status',
                    ellipsis: true,
                    width: 120,
                    filters: [
                        { text: formatMsg('STOPPED'), value: 'STOPPED' },
                        { text: formatMsg('RUNNING'), value: 'RUNNING' },
                        { text: formatMsg('STARTING'), value: 'STARTING' },
                        { text: formatMsg('STOPPING'), value: 'STOPPING' },
                    ],
                    onFilter: (value: string, record: ServerRunning) => record.status === value,
                    render: (text: string) => ServerMgrView.translateStatus(text),
                },
            ],
            loading: this.state.loading,
            dataSource: this.state.data,
            pagination: false,
            rowKey: 'sid',
            size: 'small',
            rowSelection: this.getRowSelection(),
            onRow: this.onRow.bind(this),
            showHeader: true,
            scroll: this.height,
        };
    }
    private static translateStatus(status: string) {
        let tag;
        const s = formatMsg(status);
        switch (status) {
            case JarBootConst.STATUS_STARTED:
                tag = <Tag icon={<CaretRightOutlined/>} color={"success"}>{s}</Tag>
                break;
            case JarBootConst.STATUS_STOPPED:
                tag = <Tag icon={<BorderOutlined style={{color: '#f50', background: '#f50'}}/>} color={"volcano"}>{s}</Tag>
                break;
            case JarBootConst.STATUS_STARTING:
                tag = <Tag icon={<SyncOutlined spin/>} color={"processing"}>{s}</Tag>
                break;
            case JarBootConst.STATUS_STOPPING:
                tag = <Tag icon={<SyncOutlined spin/>} color={"default"}>{s}</Tag>
                break;
            default:
                tag = <Tag color={"default"}>{s}</Tag>
                break;
        }
        return tag;
    }

    private getRowSelection() {
        return {
            type: 'checkbox',
            onChange: (selectedRowKeys: string[], selectedRows: ServerRunning[]) => {
                let current = this.state.current;
                if (!selectedRows || selectedRows.length <= 0) {
                    current = '';
                } else {
                    current = '' === current ? selectedRowKeys[0] : current;
                }
                this.setState({current, selectedRowKeys: selectedRowKeys, selectRows: selectedRows,});
            },
            selectedRowKeys: this.state.selectedRowKeys,
        };
    }

    private onRow(record: ServerRunning) {
        return {
            onClick: () => {
                this.setState({selectedRowKeys: [record.sid], selectRows: [record], current: record.sid});
            },
        };
    }

    private refreshServerList = (init: boolean = false) => {
        this.setState({loading: true});
        ServerMgrService.getServerList((resp: any) => {
            this.setState({loading: false});
            if (resp.resultCode < 0) {
                CommonNotice.errorFormatted(resp);
                return;
            }
            const data = resp.result as ServerRunning[];
            if (init) {
                //初始化选中第一个
                let current = '';
                let selectedRowKeys = [] as string[];
                let selectRows: any = [];
                if (data.length > 0) {
                    const first: ServerRunning = data[0];
                    current = first.sid ;
                    selectedRowKeys = [first.sid];
                    selectRows = [first];
                }
                this.setState({data, current, selectedRowKeys, selectRows});
                return;
            }
            this.setState({data});
        });
    };

    private finishCallback = (resp: any) => {
        this.setState({loading: false});
        if (0 !== resp.resultCode) {
            CommonNotice.errorFormatted(resp);
        }
    };

    private startServer = () => {
        if (this.state.selectRows.length < 1) {
            notSelectInfo();
            return;
        }
        this.setState({loading: true});
        this.state.selectRows.forEach(this.clearDisplay);
        ServerMgrService.startServer(this.state.selectRows, this.finishCallback);
    };

    private clearDisplay = (server: ServerRunning) => {
        pubsub.publish(server.sid, JarBootConst.CLEAR_CONSOLE);
    };

    private stopServer = () => {
        if (this.state.selectRows.length < 1) {
            notSelectInfo();
            return;
        }

        this.setState({loading: true});

        ServerMgrService.stopServer(this.state.selectRows, this.finishCallback);
    };

    private restartServer = () => {
        if (this.state.selectRows.length < 1) {
            notSelectInfo();
            return;
        }
        this.setState({loading: true});
        this.state.selectRows.forEach(this.clearDisplay);
        ServerMgrService.restartServer(this.state.selectRows, this.finishCallback);
    };

    private getTbBtnProps = () => {
        return [
            {
                name: 'Start',
                key: 'start ',
                icon: <CaretRightFilled className={styles.toolButtonGreenStyle}/>,
                onClick: this.startServer,
                disabled: !this.state.selectRows?.length
            },
            {
                name: 'Stop',
                key: 'stop',
                icon: <PoweroffOutlined className={styles.toolButtonRedStyle}/>,
                onClick: this.stopServer,
                disabled: !this.state.selectRows?.length
            },
            {
                name: 'Restart',
                key: 'restart',
                icon: <ReloadOutlined className={styles.toolButtonStyle}/>,
                onClick: this.restartServer,
                disabled: !this.state.selectRows?.length
            },
            {
                name: 'Refresh',
                key: 'refresh',
                icon: <SyncOutlined className={styles.toolButtonStyle}/>,
                onClick: () => this.refreshServerList(),
            },
            {
                name: 'New & update',
                key: 'upload',
                icon: <UploadOutlined className={styles.toolButtonStyle}/>,
                onClick: this.uploadFile,
            },
            {
                name: 'Dashboard',
                key: 'dashboard',
                icon: <DashboardOutlined className={styles.toolButtonRedStyle}/>,
                onClick: this.dashboardCmd,
                disabled: this.isCurrentNotRunning()
            }
        ]
    };

    private isCurrentNotRunning = () => {
        const item = this.state.data.find(value => value.sid === this.state.current);
        return !(item && JarBootConst.STATUS_STARTED === item.status);
    }

    private uploadFile = () => {
        this.setState({uploadVisible: true});
    };

    private dashboardCmd = () => {
        if (this.state.selectRows?.length < 1 || StringUtil.isEmpty(this.state.current)) {
            notSelectInfo();
            return;
        }
        pubsub.publish(this.state.current, PUB_TOPIC.QUICK_EXEC_CMD, "dashboard");
    };

    private oneClickRestart = () => {
        pubsub.publish(this.state.current, JarBootConst.APPEND_LINE, "Restarting all...");
        this.disableOnClickButton();
        ServerMgrService.oneClickRestart();
    };

    private oneClickStart = () => {
        pubsub.publish(this.state.current, JarBootConst.APPEND_LINE, "Starting all...");
        this.disableOnClickButton();
        ServerMgrService.oneClickStart();
    };

    private oneClickStop = () => {
        pubsub.publish(this.state.current, JarBootConst.APPEND_LINE, "Stopping all...");
        this.disableOnClickButton();
        ServerMgrService.oneClickStop();
    };

    private disableOnClickButton() {
        if (this.state.oneClickLoading) {
            return;
        }
        this.setState({oneClickLoading: true}, () => {
            const td = setTimeout(() => {
                clearTimeout(td);
                this.setState({oneClickLoading: false});
            }, 5000);
        });
    }

    private onUploadClose = () => {
        this.setState({uploadVisible: false});
        this.refreshServerList();
    };

    render() {
        let tableOption: any = this.getTbProps();
        tableOption.scroll = { y: this.height};
        const loading = this.state.loading && 0 === this.state.data.length;
        return (<div>
            <div style={{display: 'flex'}}>
                <div style={{flex: 'inherit', width: '28%'}}>
                    <CommonTable toolbarGap={5} option={tableOption}
                                 toolbar={this.getTbBtnProps()} height={this.height}/>
                    <OneClickButtons loading={this.state.oneClickLoading}
                                     oneClickRestart={this.oneClickRestart}
                                     oneClickStart={this.oneClickStart}
                                     oneClickStop={this.oneClickStop}/>
                </div>
                <div style={{flex: 'inherit', width: '72%'}}>
                    {loading && <Result icon={<LoadingOutlined/>} title={formatMsg('LOADING')}/>}
                    {<SuperPanel key={PUB_TOPIC.ROOT}
                                 server={""}
                                 sid={PUB_TOPIC.ROOT}
                                 visible={!loading && this.state.current?.length <= 0}/>}
                    {this.state.data.map((value: ServerRunning) => (
                        <SuperPanel key={value.sid}
                                    server={value.name}
                                    sid={value.sid}
                                    visible={this.state.current === value.sid}/>
                    ))}
                </div>
            </div>
            {this.state.uploadVisible && <UploadFileModal server={this.state.selectRows[0].name}
                                                          onClose={this.onUploadClose}/>}
        </div>);
    }
}
